public class Store {

    private Cash[] cashes;
    private int r;

    private Store() {
        cashes = new Cash[]{new Cash(2), new Cash(3), new Cash(1), new Cash(3)};
    }

    private void addCustomer(Customer newCustomer){
        Cash c = newCustomer.chooseCash(cashes);
        c.addInQueue(newCustomer);
    }

    private void doStep() {
        r++;
        System.out.println("Шаг "+r);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        for (Cash q : cashes) {
            q.serve();
        }
        print();
    }

    private void workStore(){
        System.out.println("Исхоное положение");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        print();
        for (int i =0; i<3; i++){
            doStep();
        }
    }

    private void print(){
        int n = 0;
        for (Cash p : cashes) {
            n++;
            System.out.println("Касса " + n +" Человек в очереди: "+p.queue.size()+" "+p.queue);
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println();
    }

    public static void main(String[] args) {

        Store store = new Store();
        store.addCustomer(new Child());
        store.addCustomer(new Child());
        store.addCustomer(new Woman());
        store.addCustomer(new Man());
        store.addCustomer(new Child());
        store.addCustomer(new Woman());
        store.addCustomer(new Man());
        store.addCustomer(new Woman());
        store.addCustomer(new Man());
        store.addCustomer(new Woman());
        store.addCustomer(new Woman());
        store.addCustomer(new Woman());

        store.workStore();
    }
}
