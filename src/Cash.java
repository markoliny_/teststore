import java.util.LinkedList;

class Cash {
    int speed;
    LinkedList<Customer> queue;

    Cash(int s) {
        speed = s;
        queue = new LinkedList<>();
    }

    void addInQueue(Customer customer) {
        queue.addLast(customer);
    }

    private int getPurchase() {
        if (queue.size() > 0) {
            Customer firstPur = queue.getFirst();
            return firstPur.purchase;
        }
        return 0;
    }

    private void setPurchase(int pur) {
        if (queue.size() > 0) {
            Customer p = queue.getFirst();
            p.purchase = pur;
        }
    }

    int getCountCust() {
        return queue.size();
    }

    void serve() {
        int remainder = speed;
        int purCount = getPurchase();

        if (purCount < speed && queue.size() > 0) {
            while ((purCount = getPurchase()) < remainder && queue.size() > 0) {
                remainder = remainder - purCount;
                queue.pop();

                if ((purCount = getPurchase()) == remainder && queue.size() > 0) {
                    queue.pop();
                    // да, я забыл записывать 0ой результат после вычита remainder
                    remainder = 0;
                } else if (purCount > remainder) {
                    setPurchase(purCount - remainder);
                    remainder = 0;
                }
            }
        } else if (purCount == speed) {
            queue.pop();
        } else if (purCount > speed) {
            setPurchase(purCount - speed);
        }
    }
}
