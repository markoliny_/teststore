import java.util.Random;

public class Child extends Customer {

    Child() {
        super();
    }

    @Override
    public Cash chooseCash(Cash[] cashes) {
        Random random = new Random();
        int rndNum = random.nextInt(cashes.length);

        return cashes[rndNum];
    }
}