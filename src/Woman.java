public class Woman extends Customer {
    Woman() {
        super();
    }

    @Override
    public Cash chooseCash(Cash[] cashes) {
        Cash shortCash = cashes[0];
        for (Cash w : cashes) {
            if (w.getCountCust() < shortCash.getCountCust()) {
                shortCash = w;
            }
        }
        return shortCash;
    }
}
