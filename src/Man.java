public class Man extends Customer {

    Man(){
        super();
    }

    @Override
    public Cash chooseCash(Cash[] cashes) {
        Cash fastestCash = cashes[0];
        for (Cash m: cashes) {
            if (m.speed > fastestCash.speed) {
                fastestCash = m;
            }
        }
        return fastestCash;
    }
}
