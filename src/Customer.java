import java.util.Random;

abstract public class Customer {
    int purchase;
    public boolean isJustComeIn;

    Customer() {
        Random rnd = new Random();
        purchase = 1 + rnd.nextInt(5);
    }

    abstract public Cash chooseCash(Cash[] array);
}
